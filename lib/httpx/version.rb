# frozen_string_literal: true

module HTTPX
  VERSION = "0.19.4"
end
